package opmq

import (
	"fmt"
	"strconv"
	"testing"
	"time"
)

func TestNewTree2(t *testing.T) {
	for {
		res := NewTreeData()
		root := NewTree()

		sub := NewClientConn("sub", nil, nil)
		AddTopic("dev/1/2/3", sub, root)
		AddTopic("dev/1/2/#", sub, root)
		AddTopic("dev/1/+/3", sub, root)

		Scan2("dev/1/2/3", root, res)

		fmt.Println(root)
		fmt.Println("???1", len(res.Res))
		fmt.Println("???2", len(res.Res))
		time.Sleep(time.Second)
	}
}

func TestAddTopic(t *testing.T) {
	gns := make([]string, 0)
	for i := 0; i < 2000; i++ {
		gns = append(gns, "sub_"+strconv.Itoa(i))
	}

	//此处做一些不会影响测试函数本身的性能的工作
	root := NewTree()

	for _, v := range gns {
		sub := NewClientConn(v, nil, nil)
		AddTopic("dev/1/2/3/4/5", sub, root)
		AddTopic("dev/a/b/c/d/e", sub, root)
		AddTopic("dev/A/B/C/D/E", sub, root)
		AddTopic("dev/x1/x1/x3/x4/x5", sub, root)

		// 添加带通配符的
		AddTopic("dev/1/+/3/+/5", sub, root)
		AddTopic("dev/a/b/+/d/#", sub, root)
		AddTopic("dev/A/+/C/D/E", sub, root)
		AddTopic("dev/+/x1/x3/x4/#", sub, root)
	}

	res := NewTreeData()
	Scan2("dev/1/2/3/4/5", root, res)
	fmt.Println(len(res.Res))
	for _, v := range res.Res {
		fmt.Println(len(v.conns))
	}
}
