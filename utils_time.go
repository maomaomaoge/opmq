package opmq

import "time"

// TimeUnixToString int64转化为时间字符串: "2006-01-02 15:04:05"
func TimeUnixToString(timeUnix int64) string {
	var (
		layout string
		res    string
	)

	layout = "2006-01-02 15:04:05"
	res = time.Unix(timeUnix, 0).Format(layout)

	return res
}

func TimeStringToUnix(s string) (time.Time, error) {
	layout := "2006-01-02 15:04:05"
	location, err := time.ParseInLocation(layout, s, time.Local)
	if err != nil {
		return location, err
	}

	return location, nil
}

// WaitDur 睡眠一段时间
func WaitDur(wait time.Duration, dur ...time.Duration) {
	tm := time.Now()
	// 结束时间
	date := tm.Add(wait)

	durNow := time.Millisecond * 300

	if len(dur) > 0 {
		durNow = dur[0]
	}

	for {
		t := time.Now()

		if t.Sub(date) >= 0 {
			return
		}
		time.Sleep(durNow)
	}
}
