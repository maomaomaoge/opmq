/*
 * Copyright (c) 2021 IBM Corp and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    https://www.eclipse.org/legal/epl-2.0/
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Seth Hoenig
 *    Allan Stockdill-Mander
 *    Mike Robertson
 */

package main

import (
	"fmt"
	//"fmt"
	"github.com/eclipse/paho.mqtt.golang"
	"time"
)

func main() {
	//mqtt.DEBUG = log.New(os.Stdout, "", 0)
	//mqtt.ERROR = log.New(os.Stdout, "", 0)
	opts := mqtt.NewClientOptions().AddBroker("tcp://192.168.20.80:1883").SetClientID("pub1")

	c := mqtt.NewClient(opts)
	token := c.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	b := make([]byte, 0)
	for i := 0; i < 1024 * 10; i++ {
		b = append(b, 255)
	}

	now := time.Now()
	// 发送带宽


	// 每秒发送
	ticker := time.NewTicker(time.Second)
	for {
		<-ticker.C
		go func() {
			for i := 0; i < 1000;i++ {
				token2 := c.Publish("/dev/41/42/43/44", 0, false, b)
				token2.Wait()
			}
		}()
	}

	fmt.Println(time.Since(now))
	for {
		time.Sleep(time.Second * 5)
	}

}
