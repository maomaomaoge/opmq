package main

import (
	"fmt"
	"strconv"
	"time"
)

func main() {
	t := time.Now()


	fmt.Println(strconv.FormatInt(t.Unix(), 10))
	fmt.Println(t.Unix())
}
