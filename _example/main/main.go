package main

import (
	"gitee.com/maomaomaoge/opmq"
	"net"
)

func main() {
	l, err := net.Listen("tcp", ":1883" )
	if err != nil {
		return
	}
	opmq.Broker = opmq.NewServer(l, "", "")
	opmq.Broker.Serve()
}
