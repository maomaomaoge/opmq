/*
 * Copyright (c) 2021 IBM Corp and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    https://www.eclipse.org/legal/epl-2.0/
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Seth Hoenig
 *    Allan Stockdill-Mander
 *    Mike Robertson
 */

package main

import (
	"github.com/eclipse/paho.mqtt.golang"
	"time"
)

var count int = 200
var dur int = 2
var jobNum int = 3

func work()  {
	//mqtt.DEBUG = log.New(os.Stdout, "", 0)
	//mqtt.ERROR = log.New(os.Stdout, "", 0)
	opts := mqtt.NewClientOptions().AddBroker("tcp://192.168.40.112:7000")

	c := mqtt.NewClient(opts)
	token := c.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	b := make([]byte, 0)
	for i := 0; i < 1024 * 1024; i++ {
		b = append(b, 255)
	}
	go func(b []byte) {
		for {
			for i := 0; i < count; i++ {
				go func() {
					token := c.Publish("qtmd", 0, false, b)
					token.Wait()
				}()
			}
			time.Sleep(time.Second * time.Duration(dur))
		}
	}(b)

	for {
		time.Sleep(time.Second)
	}
}

func main() {
	for i := 0; i < jobNum;i++ {
		go work()
	}

	for {
		time.Sleep(time.Second)
	}
}
