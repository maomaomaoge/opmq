/*
 * Copyright (c) 2021 IBM Corp and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    https://www.eclipse.org/legal/epl-2.0/
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Seth Hoenig
 *    Allan Stockdill-Mander
 *    Mike Robertson
 */

package main

import (
	"fmt"
	"github.com/eclipse/paho.mqtt.golang"
	"os"
	"strconv"
	"time"
)

var count int = 500
var record int

var f mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Println(time.Now().String())
	fmt.Println("topic： ", msg.Topic())
	fmt.Println("收到数据长度: ", len(msg.Payload()))
	record++
	fmt.Println(record)
}

func main() {
	//mqtt.DEBUG = log.New(os.Stdout, "", 0)
	//mqtt.ERROR = log.New(os.Stdout, "", 0)
	dur, err := strconv.Atoi(os.Args[1])
	if err != nil {
		return
	}
	fmt.Println("时间间隔毫秒； ", dur)

	opts := mqtt.NewClientOptions().AddBroker("tcp://192.168.40.112:1883")
	opts.SetClientID("hz_pub_always")

	c := mqtt.NewClient(opts)
	token := c.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	b := make([]byte, 0)
	for i := 0; i < 10 ; i++ {
		b = append(b, 255)
	}

	for {
		token2 := c.Publish("/dev/11/12/13", 0, false, b)
		token2.Wait()
		token2 = c.Publish("/dev/21/22/23", 0, false, b)
		token2.Wait()
		token2 = c.Publish("/dev/31/32/33", 0, false, b)
		token2.Wait()

		time.Sleep(time.Millisecond * time.Duration(dur))
	}
	//c.Disconnect(250)
}
