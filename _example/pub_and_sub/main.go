/*
 * Copyright (c) 2021 IBM Corp and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    https://www.eclipse.org/legal/epl-2.0/
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Seth Hoenig
 *    Allan Stockdill-Mander
 *    Mike Robertson
 */

package main

import (
	"fmt"
	"github.com/eclipse/paho.mqtt.golang"
	"os"
	"time"
)

var record int

var f mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Println(time.Now().String())
	fmt.Println("topic： ", msg.Topic())
	fmt.Println("收到数据长度: ", len(msg.Payload()))
	record++
	fmt.Println(record)
}

var f1 mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Println(time.Now().String(), msg.Topic(), "收到数据长度: ", len(msg.Payload()))
}

var f2 mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Println(time.Now().String(), msg.Topic(), "收到数据长度: ", len(msg.Payload()))
}

var f3 mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Println(time.Now().String(), msg.Topic(), "收到数据长度: ", len(msg.Payload()))
}

func main() {
	//mqtt.DEBUG = log.New(os.Stdout, "", 0)
	//mqtt.ERROR = log.New(os.Stdout, "", 0)
	opts := mqtt.NewClientOptions().AddBroker("tcp://192.168.20.80:7000")
	opts.SetClientID("sub_only")

	c := mqtt.NewClient(opts)
	defer func() {
		//fmt.Println("断开连接")
		//c.Disconnect(250)
	}()
	token := c.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	if token = c.Subscribe("data.io/realtime", 0, nil); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	if token = c.Subscribe("root/1", 0, f1); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	if token = c.Subscribe("root/2", 0, f2); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	if token = c.Subscribe("root/3", 0, f3); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	b := make([]byte, 0)
	for i := 0; i < 1024 * 1024; i++ {
		b = append(b, 255)
	}
	for {
		go func() {
			token = c.Publish("root/1", 0, false, b)
			token.Wait()
		}()

		go func() {
			token = c.Publish("root/2", 0, false, b)
			token.Wait()
		}()


		go func() {
			token = c.Publish("root/3", 0, false, b)
			token.Wait()
		}()
		time.Sleep(time.Second)
	}
}
