package main

import (
	"encoding/json"
	"fmt"
	"time"
)

type Point struct {
	Id int32
	GN string
	Tm float64
	Av interface{}
}

// 200测点10k
func main() {
	data := make([]*Point, 0)
	t := float64(time.Now().Unix())
	for i := 0 ; i < 200;i++ {
		data = append(data, &Point{
			Id: int32(i),
			GN: "W3.NODE.P1",
			Tm: t,
			Av: 10,
		})
	}

	bytes, e := json.Marshal(data)
	if e != nil {
		return
	}
	//10491 约等于 10K
	fmt.Println("字节数；", len(bytes))
}
