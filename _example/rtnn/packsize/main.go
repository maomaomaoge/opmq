package main

import (
	"encoding/json"
	"fmt"
	"time"
)

var count int = 100000

type OpPoint struct {
	EC    int32
	Id    int32
	Name  string
	Tm    float64
	Ds    uint16
	Value int
}

func main() {
	t := time.Now().Unix()
	data := make([]byte, 0)
	for i := 0; i < count;i++ {
		rtnn := &OpPoint{
			Id: 1,
			Name: "W3.TEST.TEST",
			Tm: float64(t),
			Value: 1,
		}
		marshal, err := json.Marshal(rtnn)
		if err != nil {
			fmt.Println(err)
			return
		}

		data = append(data, marshal...)
	}

	fmt.Println(float64(len(data)) / 1024 / 1024, "/mb")
}

