package main

import (
	"encoding/json"
	"fmt"
	"github.com/eclipse/paho.mqtt.golang"
	"time"
)

var count int = 10000
var record int

type OpPoint struct {
	EC    int32
	Id    int32
	Name  string
	Tm    float64
	Ds    uint16
	Value int
}

func mockData() ([]byte, float64) {
	t := time.Now().Unix()
	rtnnData := make([]*OpPoint, 0)
	for i := 0; i < count;i++ {
		rtnn := &OpPoint{
			Id: 1,
			Name: "W3.TEST.TEST",
			Tm: float64(t),
			Value: 1,
		}
		rtnnData = append(rtnnData, rtnn)
	}

	marshal, err := json.Marshal(rtnnData)
	if err != nil {
		fmt.Println(err)
		return nil, 0
	}

	fmt.Println(float64(len(marshal)) / 1024 / 1024, "/mb")

	return marshal, float64(len(marshal)) / 1024 / 1024
}


func main() {
	//mqtt.DEBUG = log.New(os.Stdout, "", 0)
	//mqtt.ERROR = log.New(os.Stdout, "", 0)
	data, size := mockData()
	opts := mqtt.NewClientOptions().AddBroker("tcp://127.0.0.1:1883")

	c := mqtt.NewClient(opts)
	token := c.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	ticker := time.NewTicker(time.Second)
	for {
		t := <-ticker.C
		record++
		fmt.Println("send num: ", record, " 数据包大小 /mb: ", size, " 时间戳: ", t.String())
		go func(data []byte) {
			token := c.Publish("data.io/realtime", 0, false, data)
			token.Wait()
		}(data)
	}
}
