package opmq

import (
	"fmt"
	"os"
	"time"
)

type WheelArgData struct {
	Key      string
	Val      interface{}
	Interval time.Duration // 秒
}

// 轮训任务了
// 对时间刻度要求不高
var TW *TimeWheel

func NewTW() *TimeWheel {
	// 初始化时间轮
	// 第一个参数为tick刻度, 即时间轮多久转动一次
	// 第二个参数为时间轮槽slot数量
	// 第三个参数为回调函数
	tw := New(1*time.Second, 3600, func(data interface{}) {
		argData, ok := data.(*WheelArgData)
		if !ok {
			DEBUG.Println("时间轮不支持的类型")
			return
		}

		if argData.Interval == 0 {
			return
			ERROR.Println("时间间隔不能为0")
			return
		}

		switch argData.Val.(type) {
		case *os.File:
			f := argData.Val.(*os.File)
			//DEBUG.Println(argData.Key + "进行系统刷盘")

			f.Sync()
			//DEBUG.Println(f.Name(), "磁盘信息: ")
			info, err := f.Stat()
			if err != nil {
				ERROR.Println(err)
				return
			}
			fmt.Println("size: ", info.Size()/1024, " kb")
			fmt.Println("modetime: ", info.ModTime())
		default:
			DEBUG.Println("不支持的时间轮任务类型")
		}

		TW.AddTimer(argData.Interval, argData.Key, data)
	})

	tw.Start()

	return tw
}
