package opmq

import (
	_ "gitee.com/maomaomaoge/opmq/docs"
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"io"
	"os"
	"time"
)

func ApiInitRouter() *gin.Engine {
	// Disable Console Color, you don't need console color when writing the logs to file.
	gin.DisableConsoleColor()

	os.Mkdir("logs", os.ModePerm)
	f, _ := os.OpenFile("logs/rest_api.log", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0660)
	dur := time.Minute * 7
	TW.AddTimer(dur, "dashboard", &WheelArgData{
		Key:      "apilog",
		Val:      f,
		Interval: dur,
	})

	gin.DefaultWriter = io.MultiWriter(f)

	r := gin.Default()
	// 全局中间件
	r.Use(Cors())

	// 8081的主路由
	r.GET("/", rootHttp)

	apiv4 := r.Group("/api/v4")
	{
		apiv4.GET("/clients", V4clients)
		apiv4.GET("/sub", V4Subscriptions)
	}

	cluster := r.Group("/cluster")
	{
		node := cluster.Group("/node")
		{
			// 集群加入一个节点
			node.GET("/join", NodeJoin)

			node.GET("/ctl", ClusterCtl)
		}
	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	return r
}
