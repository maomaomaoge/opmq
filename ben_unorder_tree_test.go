package opmq

import (
	"strconv"
	"testing"
)

// 模拟2000个盒子，topic每个盒子10个相同的topic，有5个是不带通配符的5个带的
//
//goos: windows
//goarch: amd64
//pkg: gitee.com/maomaomaoge/opmq
//cpu: Intel(R) Core(TM) i5-10200H CPU @ 2.40GHz
//BenchmarkScan2
//BenchmarkScan2-8   	  227024	      5331 ns/op
//PASS
func BenchmarkScan2(b *testing.B) {
	b.StopTimer() //调用该函数停止压力测试的时间计数
	gns := make([]string, 0)
	for i := 0; i < 2000; i++ {
		gns = append(gns, "sub_"+strconv.Itoa(i))
	}

	//此处做一些不会影响测试函数本身的性能的工作
	root := NewTree()

	for _, v := range gns {
		sub := NewClientConn(v, nil, nil)
		AddTopic("dev/1/2/3/4/5", sub, root)
		AddTopic("dev/a/b/c/d/e", sub, root)
		AddTopic("dev/A/B/C/D/E", sub, root)
		AddTopic("dev/x1/x1/x3/x4/x5", sub, root)

		// 添加带通配符的
		AddTopic("dev/1/+/3/+/5", sub, root)
		AddTopic("dev/a/b/+/d/#", sub, root)
		AddTopic("dev/A/+/C/D/E", sub, root)
		AddTopic("dev/+/x1/x3/x4/#", sub, root)
	}

	b.StartTimer()             //重新开始时间
	for i := 0; i < b.N; i++ { //use b.N for looping
		res := NewTreeData()
		Scan2("dev/1/2/3/4/5", root, res)
	}
}
