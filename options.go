package opmq

type Options func(b *Server)

// WithAuth 开启用户名密码
func WithAuth(user, pwd string) Options {
	return func(b *Server) {
		b.Auth = NewAuth(user, pwd)
	}
}

// WithHTTP dashboard和api的端口号
func WithHTTP(dashboard string, api string) Options {
	return func(b *Server) {
		if dashboard == "" || api == "" {
			DEBUG.Println("dashboard端口号为空,采取默认配置: 18083, http API 端口为: 8081")
			httpServer := NewHttpServer("18083", "8081")
			b.HttpServer = httpServer
			return
		}

		DEBUG.Println("dashboard的端口号为: ", dashboard)
		DEBUG.Println("http api端口号为: ", api)
		httpServer := NewHttpServer(dashboard, api)
		b.HttpServer = httpServer
	}
}

// WithMqttPort 本节点的mq端口号
func WithMqttPort(port string) Options {
	return func(b *Server) {
		b.MqttPort = port
	}
}

// WithClusterId 集群节点id
func WithClusterId(id string) Options {
	return func(b *Server) {
		b.ClusterSelf.Id = id
	}
}

// WithIsMaster 是否是集群主节点
func WithIsMaster(f string) Options {
	return func(b *Server) {
		if f == "master" {
			DEBUG.Println("开启主节点模式")
			b.ClusterSelf.IsMaster = true
		} else {
			DEBUG.Println("开启从节点模式")
			b.ClusterSelf.IsMaster = false
		}
	}
}

// WithClusterIp 集群节点ip
func WithClusterIp(ip string) Options {
	return func(b *Server) {
		b.ClusterSelf.ClusterNet.Ip = ip
	}
}

// WithClusterRemoteMasterIp 如果当前节点是从节点，远程主节点ip + 端口号
func WithClusterRemoteMasterIp(s string) Options {
	return func(b *Server) {
		b.ClusterSelf.RemoteMasterIp = s
	}
}

// WithClusterRemoteMasterPort 主节点的端口号
func WithClusterMasterRpcPort(port string) Options {
	return func(b *Server) {
		b.ClusterSelf.MasterRocPort = port
	}
}

// WithClusterRpcPort rpcport初始化
func WithClusterRpcPort(port string) Options {
	return func(b *Server) {
		b.ClusterSelf.ClusterNet.RpcPort = port
	}
}
