package opmq

import (
	"log"
	"os"
)

type (
	// Logger interface allows implementations to provide to this package any
	// object that implements the methods defined in it.
	Logger interface {
		Println(v ...interface{})
		Printf(format string, v ...interface{})
	}

	// NOOPLogger implements the logger that does not perform any operation
	// by default. This allows us to efficiently discard the unwanted messages.
	NOOPLogger struct{}
)

func (NOOPLogger) Println(v ...interface{})               {}
func (NOOPLogger) Printf(format string, v ...interface{}) {}

// Internal levels of library output that are initialised to not print
// anything but can be overridden by programmer
var (
	ERROR                Logger = NOOPLogger{}
	CRITICAL             Logger = NOOPLogger{}
	WARN                 Logger = NOOPLogger{}
	DEBUG                Logger = NOOPLogger{}
	PANIC                Logger = NOOPLogger{}
	DEBUG_TREE           Logger = NOOPLogger{}
	DEBUG_HTTP_DASHBOARD Logger = NOOPLogger{}
	DEBUG_PUBLISH        Logger = NOOPLogger{}
	CLUSTER_DEBUG        Logger = NOOPLogger{}
	CLUSTER_PANIC        Logger = NOOPLogger{}
)

func init() {
	// 复杂的树调试
	//DEBUG_TREE = log.New(os.Stdout, "[DEBUG_TREE] ", log.Llongfile | log.Ltime)
	DEBUG = log.New(os.Stdout, "[DEBUG] ", log.Llongfile|log.Ltime)

	PANIC = log.New(os.Stdout, "[PANIC]", log.Llongfile|log.Ltime)

	ERROR = log.New(os.Stdout, "[ERR]", log.Llongfile|log.Ltime)

	DEBUG_HTTP_DASHBOARD = log.New(os.Stdout, "[DASH_DEBUG] ", log.Llongfile|log.Ltime)

	// 高频发布数据调试日志
	//DEBUG_PUBLISH = log.New(os.Stdout, "[DASH_DEBUG] ", log.Llongfile|log.Ltime)

	CLUSTER_DEBUG = log.New(os.Stdout, "[CLUSTER_INFO]", log.Llongfile|log.Ltime)
	CLUSTER_PANIC = log.New(os.Stdout, "[CLUSTER_PANIC]", log.Llongfile|log.Ltime)
}
