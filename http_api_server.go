package opmq

import (
	"context"
	"net/http"
	"time"
)

// dashboard的http server
type HttpApiServer struct {
	Srv *Server

	Hsrv *http.Server
}

func NewHttpApiServer(port string) *HttpApiServer {
	srv := &http.Server{
		Addr:    ":" + port,
		Handler: ApiInitRouter(),
	}

	hs := &HttpApiServer{
		Hsrv: srv,
	}

	return hs
}

func (h *HttpApiServer) Serve() {
	err := h.Hsrv.ListenAndServe()
	if err != nil {
		DEBUG.Println("http api服务关闭")
	}
}

func (h *HttpApiServer) ShoutDown() {
	DEBUG.Println("开始关闭http api")
	h.Hsrv.Shutdown(context.Background())
	time.Sleep(time.Millisecond * 300)
}
