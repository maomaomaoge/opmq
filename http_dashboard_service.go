package opmq

import (
	"github.com/gin-gonic/gin"
	"os"
)

type Card4 struct {
	Pid     int   `json:"pid"`      // 程序pid
	SubNum  int   `json:"sub_num"`  // 订阅数, 也就是主题数量
	ConnNum int64 `json:"conn_num"` // 连接数
}

type DhbClient struct {
	ClientId  string `json:"client_id"`  // 客户端id
	User      string `json:"user"`       // 客户端用户名
	Ip        string `json:"ip"`         // 客户端ip
	Heartbeat int    `json:"heartbeat"`  // 心跳 /s
	IsAlive   string `json:"is_alive"`   // 连接状态
	ConnectAt string `json:"connect_at"` // 连接时间

	SubList []string `json:"sub_list"` // 订阅的主题
}

// dhbMonitorCard4 godoc
// @Summary 监控卡片的四个统计
// @Description 获取四个指标
// @Success 200 {array} Card4
// @Router /dhb_monitor/card4 [get]
func dhbMonitorCard4(c *gin.Context) {
	res := &Card4{}
	res.Pid = os.Getpid()
	for _, v := range Broker.ClientConns {
		res.SubNum += len(v.SubList)
	}
	res.ConnNum = int64(len(Broker.GetAllClientData()))
	c.JSON(200, res)
}

// dhbClient godoc
// @Summary 监控卡片的四个统计
// @Description 获取所有客户端
// @Success 200 {array} DhbClient
// @Router /dhb_client/table_data [get]
func dhbClient(c *gin.Context) {
	res := make([]*DhbClient, 0)
	for _, v := range Broker.ClientConns {
		d := &DhbClient{
			ClientId:  v.Cid,
			User:      "",
			Ip:        v.ClientsProperty.IpAddress,
			Heartbeat: int(v.ClientsProperty.Keepalive),
			ConnectAt: v.ClientsProperty.ConnectedAt,
			SubList:   make([]string, 0),
		}
		if v.IsAlive == true {
			d.IsAlive = "已连接"
		} else {
			d.IsAlive = "掉线"
		}

		d.SubList = v.SubList
		res = append(res, d)
	}

	c.JSON(200, gin.H{
		"dc": res,
	})
}
