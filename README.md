[TOC]

## opmq

分布式`mqtt`消息队列



#### swag

> swagger/index.html



#### 前端dashboard地址

> https://gitee.com/maomaomaoge/vopmq





## 架构

master节点只是负责数据topic缓存

slave节点进行数据订阅和发布



#### 集群

##### 节点数据同步方式

> 最终选择ticker时间间隔进行push， pull

##### 事件驱动的优缺点

> 优点是响应及时
>
> 缺点是: 客户端过多，并且订阅列表是一个个上来的，1000个client，每个client有10个主题的话，那掉线重现的网络和cpu频率太大

##### ticker push pull

> 优点是： 可以减轻网络频率
>
> 缺点： 掉线重连响应不及时





```sh
protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative \
    routeguide/route_guide.proto
```



## TODO

1. 节点健康监测
2. 主节点备份
3. 主节点重新定位