package opmq

type Auth struct {
	IsUsed bool
	User   string
	Pwd    string
}

func NewAuth(u, p string) *Auth {
	a := &Auth{
		IsUsed: false,
	}
	if u == "" || p == "" {
		DEBUG.Println("不开启用户名和密码")
		return a
	}

	a.User = u
	a.Pwd = p
	a.IsUsed = true
	DEBUG.Println("开启的用户名密码为: ", a.User, a.Pwd)

	return a
}
