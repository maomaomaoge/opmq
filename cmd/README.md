[TOC]

## opmq

一款简单的mqtt server端实现，开箱即用， 不支持分布式，单机效率最大化，速度和cpu和网卡有关



- go sdk: `github.com/eclipse/paho.mqtt.golang`
- 其他语言，只要符合了 `MQTT 3.1/3.11`就可以进行通讯



## Installation and Build

###### 源码编译

- go version:  `go1.16.5 windows/amd64`

- 拉取仓库，`进入到cmd目录`， 直接运行go build 

  > windows: go build -o mqctl.exe
  >
  > linux: go build -o mqctl



## CLI

> 下面都是执行命令

##### 获取帮助

> mqctl.exe

```
cli of opmq

Usage:
  mqctl [command]

Available Commands:
  completion  generate the autocompletion script for the specified shell
  help        Help about any command
  pub         发布
  start       start short
  sub         订阅

Flags:
  -h, --help   help for mqctl

Use "mqctl [command] --help" for more information about a command.
```



##### 开启server

> mqctl.exe start -p 1883

###### 帮助命令: 
>  mqctl.exe start -h

```
start short

Usage:
  mqctl start [flags]

Examples:

        # 如果不添加参数，默认为1883端口
        mqctl start -p 1883


Flags:
  -h, --help          help for start
  -p, --port string   启动的端口
```



#### 开启订阅

> mqctl.exe sub -c 1 -t qtmd -i 127.0.0.1:1883

帮助命令: 

> mqctl.exe sub -h

```
  mqctl pub [flags]

Examples:

        # 开启3个tcp客户端，每条消息包大小100kb， 时间间隔为1毫秒, 每个tcp最多发300条结束， topic: qtmd
        mqctl pub -c 3 -e 100 -d 1 -t 300 -i 127.0.0.1:1883


Flags:
  -c, --cn int         mock num of tcp conn
  -d, --dur int        mock time dur -> ms
  -e, --ep int         mock size of  every tcp pack -> kb
  -h, --help           help for pub
  -i, --ip string      访问的ip
  -o, --topic string   topic
  -t, --total int      每个线程发送的消息条数
```



#### 开启发布

> pub -o qtmd  -c 3 -e 100 -d 1 -t 300 -i 127.0.0.1:1883

###### 帮助命令:

> mqctl.exe pub -h

```
发布

Usage:
  mqctl pub [flags]

Examples:

        # 开启3个tcp客户端，每条消息包大小100kb， 时间间隔为1毫秒, 每个tcp最多发300条结束
        mqctl pub -c 3 -e 100 -d 1 -t 300 -i 127.0.0.1:1883


Flags:
  -c, --cn int         mock num of tcp conn
  -d, --dur int        mock time dur -> ms
  -e, --ep int         mock size of  every tcp pack -> kb
  -h, --help           help for pub
  -i, --ip string      访问的ip
  -o, --topic string   topic
  -t, --total int      每个线程发送的消息条数
```



## TODO

- [ ] topic选型为字典树，而不是btree
- [ ] 健康监测
