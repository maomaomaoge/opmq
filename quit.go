package opmq

import (
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"time"
)

func WaitSignal(srv *Server) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGINT)
	var s os.Signal
	for {
		select {
		case s = <-c:
			DEBUG.Println("got os signal ", s.String())
			srv.HttpServer.Stop()
			time.Sleep(time.Millisecond * 100)

			DEBUG.Println("关闭轮询任务时间轮")
			TW.Stop()
			time.Sleep(time.Millisecond)

			exec.Command("sync").Run()
			return
		default:
			time.Sleep(time.Second)
		}
	}

}
