package opmq

import "github.com/spf13/viper"

func DeWeight(data []string) []string {
	res := make([]string, 0)

	m := make(map[string]int)

	for _, v := range data {
		_, ok := m[v]
		if ok {
			continue
		}
		m[v] = 0

		res = append(res, v)
	}

	return res
}

func Viper(path string) (*viper.Viper, error) {
	v := viper.New()

	v.SetConfigFile(path)
	err := v.ReadInConfig()
	if err != nil {
		return nil, err
	}

	return v, nil
}
