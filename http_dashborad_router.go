package opmq

import (
	"fmt"
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"io"
	"net/http"
	"os"
	"time"

	//"net/http"

	_ "gitee.com/maomaomaoge/opmq/docs"
)

// 简单ui文档
// https://docs.emqx.cn/enterprise/v4.3/getting-started/dashboard-ee.html#%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8
func DashboardInitRouter() *gin.Engine {
	// Disable Console Color, you don't need console color when writing the logs to file.
	gin.DisableConsoleColor()

	os.Mkdir("logs", os.ModePerm)
	f, _ := os.OpenFile("logs/dashboard.log", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0660)
	dur := time.Minute * 5
	TW.AddTimer(dur, "dashboard", &WheelArgData{
		Key:      "dashboard",
		Val:      f,
		Interval: dur,
	})

	gin.DefaultWriter = io.MultiWriter(f)

	//r := gin.New()
	r := gin.Default()
	// 全局中间件
	r.Use(Cors())

	r.StaticFS("/d", http.Dir("./dash"))

	// 18083的主路由
	apiv4 := r.Group("/dhb_monitor")
	{

		// 上面四個卡片，目前是两个，消息订阅和连接数
		apiv4.GET("/card4", dhbMonitorCard4)

	}

	apiv_client := r.Group("/dhb_client")
	{

		// 上面四個卡片，目前是两个，消息订阅和连接数
		apiv_client.GET("/table_data", dhbClient)

	}

	r.GET("/", func(c *gin.Context) {
		fmt.Println("路由重新定向2")
		c.Request.URL.Path = "/d" //把请求的URL修改
		r.HandleContext(c)        //继续后续处理
	})

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	return r
}
