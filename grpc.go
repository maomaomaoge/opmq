package opmq

import (
	"fmt"
	"gitee.com/maomaomaoge/opmq/grpc_proto"
	"google.golang.org/grpc"
	"net"
)

func (s *Server) Grpc() {
	server := grpc.NewServer()
	grpc_proto.RegisterOpmqRpcServer(server, &OpmqRpc{})

	DEBUG.Println("grpc端口: ", Broker.ClusterSelf.ClusterNet.RpcPort)

	lis, err := net.Listen("tcp", ":"+Broker.ClusterSelf.ClusterNet.RpcPort)
	if err != nil {
		fmt.Println("grpc启动失败")
		return
	}

	server.Serve(lis)
}
