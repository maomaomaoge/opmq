package opmq

import (
	"fmt"
	"testing"
	"time"
)

func TestNewTree(t *testing.T) {
	// 初始化时间轮
	// 第一个参数为tick刻度, 即时间轮多久转动一次
	// 第二个参数为时间轮槽slot数量
	// 第三个参数为回调函数
	tw := New(1*time.Second, 6, func(data interface{}) {
		fmt.Println(time.Now())
		//wheel := data.(*TimeWheel)
		//wheel.AddTimer(2 * time.Second, "aaa", wheel)
	})

	// 启动时间轮
	tw.Start()

	conn := "key1"

	// 添加定时器
	// 第一个参数为延迟时间
	// 第二个参数为定时器唯一标识, 删除定时器需传递此参数
	// 第三个参数为用户自定义数据, 此参数将会传递给回调函数, 类型为interface{}
	tw.AddTimer(2*time.Second, conn, tw)

	for {
		time.Sleep(time.Second)
	}
}
