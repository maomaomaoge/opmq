package opmq

import (
	"context"
	"net/http"
	"time"
)

//https://docs.emqx.cn/enterprise/v4.3/getting-started/dashboard-ee.html#%E5%AE%A2%E6%88%B7%E7%AB%AF

// dashboard的http server
type HttpDashboardServer struct {
	Srv *Server

	Hsrv *http.Server
}

func NewHttpDashboardServer(port string) *HttpDashboardServer {
	srv := &http.Server{
		Addr:    ":" + port,
		Handler: DashboardInitRouter(),
	}

	hs := &HttpDashboardServer{
		Hsrv: srv,
	}

	return hs
}

type JsonData struct {
	Topic_ClientIds map[string][]string `json:"topic_client_ids"`
}

func (h *HttpDashboardServer) Serve() {
	err := h.Hsrv.ListenAndServe()
	if err != nil {
		DEBUG.Println("dashbord 关闭")
	}
}

func (h *HttpDashboardServer) ShoutDown() {
	DEBUG.Println("开始关闭http dashboard")
	h.Hsrv.Shutdown(context.Background())
	time.Sleep(time.Millisecond * 100)
}
