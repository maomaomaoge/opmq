package opmq

import "time"

//1883	MQTT 协议端口
//8883	MQTT/SSL 端口
//8083    MQTT/WebSocket 端口
//8080    HTTP API 端口
//18083   Dashboard 管理控制台端口

type HttpServer struct {
	Srv *Server

	Port string

	HttpDashboardServer *HttpDashboardServer
	HttpApiServer       *HttpApiServer
}

func NewHttpServer(dashPort string, apiPort string) *HttpServer {
	hs := &HttpServer{
		HttpDashboardServer: NewHttpDashboardServer(dashPort),
		HttpApiServer:       NewHttpApiServer(apiPort),
	}

	go hs.Start()
	return hs
}

func (hs *HttpServer) Start() {
	// 先让Broker装弹
	time.Sleep(time.Second * 3)

	// 操作api
	go hs.HttpApiServer.Serve()

	// 面板的api
	go hs.HttpDashboardServer.Serve()
}

func (hs *HttpServer) Stop() {
	hs.HttpApiServer.ShoutDown()
	hs.HttpDashboardServer.ShoutDown()
}
