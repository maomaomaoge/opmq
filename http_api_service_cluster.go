package opmq

import (
	"github.com/gin-gonic/gin"
)

type CodeRes struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

// NodeJoin  godoc
// @Summary join节点
// @Description must be id, ip, port
// @Param        id    query     string  false  "id in"
// @Param        ip    query     string  false  "ip in"
// @Param        port    query     string  false  "port in"
// @Success 200 {array} CodeRes
// @Router /cluster/node/join [get]
func NodeJoin(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(200, CodeRes{
			Code: -1,
			Msg:  "id不能为空",
		})
	}
	ip := c.Query("ip")
	if ip == "" {
		c.JSON(200, CodeRes{
			Code: -1,
			Msg:  "ip不能为空",
		})
	}
	port := c.Query("port")
	if port == "" {
		c.JSON(200, CodeRes{
			Code: -1,
			Msg:  "port不能为空",
		})
	}

	DEBUG.Println("id, ip, port: ", id, ip, port)

	Broker.ClusterSelf.NodeJoin(ip, port, id, []string{})
	c.JSON(200, CodeRes{
		Code: 0,
		Msg:  "ok",
	})
}

// PushNodeTopic godoc
// @Summary 推送主节点数据
// @Description 主节点数据同步自身,是往主节点推送数据，更新操作以后做
// @Param        id    query     string  false  "id in"
// @Param        ip    query     string  false  "ip in"
// @Param        port    query     string  false  "port in"
// @Param        topic    query     []string  false  "推送的topic列表"
// @Success 200 {array} CodeRes
// @Router /cluster/node/push [get]
func PushNodeTopic(c *gin.Context) {
	id := c.Query("id")
	if id == "" {
		c.JSON(200, CodeRes{
			Code: -1,
			Msg:  "id不能为空",
		})
	}
	ip := c.Query("ip")
	if ip == "" {
		c.JSON(200, CodeRes{
			Code: -1,
			Msg:  "ip不能为空",
		})
	}
	port := c.Query("port")
	if port == "" {
		c.JSON(200, CodeRes{
			Code: -1,
			Msg:  "port不能为空",
		})
	}
	topic := c.QueryArray("topic")
	Broker.ClusterSelf.NodeJoin(ip, port, id, topic)

	c.JSON(200, CodeRes{
		Code: 0,
		Msg:  "ok",
	})
}

// ClusterCtl godoc
// @Summary 通知节点去主动推送到到主节点,直接发送到系统topic吧
// @Description 自身节点推送
// @Param        lip    query     string  false  "本地ip "
// @Param        lport    query     string  false  "本地port"
// @Param        rip    query     string  false  "master ip"
// @Param        rport    query     string  false  "master port"
// @Success 200 {array} CodeRes
// @Router /cluster/node/ctl [get]
func ClusterCtl(cg *gin.Context) {
	//lip := cg.Query("lip")
	//if lip == "" {
	//	cg.JSON(200, CodeRes{
	//		Code: -1,
	//		Msg:  "ip不能为空",
	//	})
	//}
	//lport := cg.Query("lport")
	//if lport == "" {
	//	cg.JSON(200, CodeRes{
	//		Code: -1,
	//		Msg:  "port不能为空",
	//	})
	//}
	//rip := cg.Query("rip")
	//if rip == "" {
	//	cg.JSON(200, CodeRes{
	//		Code: -1,
	//		Msg:  "ip不能为空",
	//	})
	//}
	//rport := cg.Query("rport")
	//if rport == "" {
	//	cg.JSON(200, CodeRes{
	//		Code: -1,
	//		Msg:  "port不能为空",
	//	})
	//}
	//
	//CLUSTER_DEBUG.Println("ctl 收到主动推送的主节点数据操作, 本机信息: ", lip, lport, " master信息: ", rip, rport)
	//
	//opts := mqtt.NewClientOptions().AddBroker("tcp://" + rip  + ":" + rport)
	////opts.SetAutoReconnect(true)
	//opts.SetKeepAlive(time.Second * 120)
	//opts.SetClientID(time.Now().String())
	//
	//c := mqtt.NewClient(opts)
	//token := c.Connect()
	//defer c.Disconnect(250)
	//if token.Wait() && token.Error() != nil {
	//	CLUSTER_PANIC.Println("建立mq连接失败", token.Error())
	//	return
	//}
	//
	//marshal, err := json.Marshal(ClusterNode{
	//	Id:     Broker.ClusterSelf.Id,
	//	Ip:     lip,
	//	Port:   Broker.MqttPort,
	//	Topics: Broker.GetUniqueSublist(),
	//})
	//if err != nil {
	//	CLUSTER_PANIC.Println(err)
	//	return
	//}
	//
	//c.Publish(Broker.SysTopic.SYSNodeIn, 0, false, marshal)
	cg.JSON(200, CodeRes{
		Code: 0,
		Msg:  "ok",
	})
}
